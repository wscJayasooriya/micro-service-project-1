package com.sandun.microservices.Controller;

import com.sandun.microservices.Entity.Department;
import com.sandun.microservices.Service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/departments")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @PostMapping(value = "/save")
    public Department saveDepartment (@RequestBody Department department){
        return departmentService.saveDepartment(department);
    }

    @GetMapping("/get/{id}")
    public Department findDepartmentById(@PathVariable ("id") Long departmentId){
        return departmentService.findDepartmentById(departmentId);
    }
}
